from refactored_code import *
normNames = glob.glob(os.path.join('normalised_counts', '*norm.csv'))
normalisedCounts = [make_df_normCounts(normName) for normName in normNames]
phostransList = []
for i in range(len(normalisedCounts)):
    try:  
        phostransList.insert(i, search_by_function('phosphatidyltransferase',normalisedCounts[i], 'class', 'vibhuc@me.com'))
        time.sleep(300)
        phostransList[i].to_csv("sample_class"+str(i)+".csv")
    except:
        time.sleep(500)
