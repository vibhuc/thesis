import glob
import math
import numpy as np
import pandas as pd
import sys
import os
import errno
from skbio.stats import subsample_counts
from functools import reduce
from Bio import Entrez
import time
####CREATE PATH IN DIRECTORY##
def create_path(path):
    try:
        os.makedirs(path)
    except OSError as exception:
        if exception.errno != errno.EEXIST:
            raise

####READ CSV FILE INTO PANDAS DATAFRAME####
def make_df(filename):
    df = pd.read_csv(filename, header=None)
    return df

###
def make_df_normCounts(filename):
    df = pd.read_csv(filename) #index_col=[0,1]
    return df
    
####MERGE TWO DATAFRAMES####
def merge_dfs(ldf, rdf):
    print("merging...")
    return ldf.join(rdf, how='outer')


###KEEP ANNOTATIONS WITH MAX PERCENT IDENTITY### 
def keep_max_pctID_only(table):
    mask = table.groupby('query sequence id').agg('idxmax')
    maxPctID = table.loc[mask['percentage identity']].reset_index()
    return maxPctID

###MERGE FUNCTION AND ORGANISM ANNOTATION TABLES###
def merge_annotation_tables(table1, table2):
    mergedTable = pd.merge(table1, table2, on = 'query sequence id').sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y'], ascending = [0,0])
    groupByQuery = mergedTable.groupby('query sequence id')
    return mergedTable

###GET RAW COUNTS FOR MERGED ANNOTATION TABLES###
def get_and_save_counts(mergedTable, groupName, sampleID):
    counts1 = mergedTable.sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).groupby(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).size()
    newFileName = '%(group)s_%(sample)s.csv' % \
        {"group": groupName, "sample": sampleID}
    create_path('counts')
    counts1.to_csv(os.path.join('counts', newFileName)) 
    counts = pd.read_csv(os.path.join('counts', newFileName)) 
    counts.columns = ['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y', 'number of hits']
    return counts
    
####GO FROM TWO TABLES (FXN AND ORG) TO COUNTS####
def counts_within_metagenome(fxnTable, orgTable, groupName, sampleID):
    mergedTable = merge_annotation_tables(fxnDataMax, orgDataMax)
    counts = get_and_save_counts(mergedTable, groupName, sampleID)
    return counts


#####CONVERTING TAB ANNOTATIONS FROM MG-RAST INTO RAW COUNTS DATA#####
def get_counts_within_metagenome(fxnTable, orgTable, groupName, sampleID):
    fxnData = pd.read_csv(fxnTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])
    orgData = pd.read_csv(orgTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])

    fxnDataMax = keep_max_pctID_only(fxnData)
    orgDataMax = keep_max_pctID_only(orgData)

    counts = counts_within_metagenome(fxnDataMax, orgDataMax, groupName, sampleID)
    
    return counts

###FIND SAMPLING DEPTH FOR SUBSAMPLING###
def find_sampling_depth(rawCounts):
    sums = []    
    for i in range(len(rawCounts)):
        rawCounts[i] = rawCounts[i].set_index([0, 1])
        sums.insert(i, int(rawCounts[i].sum()))
    lenSeries = pd.Series(sums)
    samplingDepth = int(lenSeries.median())
    return samplingDepth

####WRITE RAREFIED AND RECODED COUNTS TO FILE####
def rarefy_and_recode(filenames, rawCounts, samplingDepth):
    for i in range(len(rawCounts)):
        subsampleList = []
        if int(rawCounts[i].sum()) < samplingDepth:
            meanSubsample = rawCounts[i]
        else:
            for j in range(100):
                sample = subsample_counts(rawCounts[i][2], samplingDepth)
                subsampleList.insert(j, sample)
            print("completed 100 subsamples for sample number " + str(i))
            meanSubsample = pd.Series(subsampleList).mean()        
        #recodification: setting all values less than 1.01 to zero
        meanSubsample[meanSubsample < 1.01] = 0
        rawCounts[i][2] = meanSubsample
        newFileName = filenames[i].split('/')[1].split('.')[0] + "_norm.csv"
        rawCounts[i].to_csv(os.path.join('normalised_counts', newFileName))
        print("written " + newFileName + " to file.")
    return

#####NORMALISATION BY SUB-SAMPLING#####
def rarefaction_and_recodification(pathToCountFiles):
    path = pathToCountFiles
    filenames = glob.glob(os.path.join(path, '*.csv'))
    rawCounts = [make_df(filename) for filename in filenames]
    create_path('normalised_counts')
    #find median of all sample counts and set as sampling depth
    samplingDepth = find_sampling_depth(rawCounts)
    #find mean of 100x subsampled counts data 
    rarefy_and_recode(filenames, rawCounts, samplingDepth)
    normNames = glob.glob(os.path.join('normalised_counts', '*norm.csv'))
    normalisedCounts = [make_df_normCounts(normName) for normName in normNames]
    return normalisedCounts
    

####GET ENTREZ TAXID FROM SPECIES NAME####
def get_taxid(species):
    """to get data from ncbi taxomomy, we need to have the taxid.  we can get that by passing the species name to esearch, which will return the tax id"""
    species = species.split(';')[0].replace(" ", "+").split('+')
    if species[0] == 'Candidatus':
        species = species[0] + ' ' + species[1]
    elif species[1] == 'proteobacterium' or species[1] == 'actinobacterium':
        species = species[0] + ' ' + species[1] + ' ' + species[2]
    elif species == ['uncultured', 'bacterium']:
        species = species[0] + ' ' + species[1]
    elif ('marine' in species) or ('uncultured' in species):
        species = ' '.join(species)
    else:
        species = species[0]
    search = Entrez.esearch(term = species, db = "taxonomy", retmode = "xml", usehistory = "y")
    record = Entrez.read(search)
    if (record['IdList'] == []):
        name_fixed = record['ErrorList']['PhraseNotFound'][0].split("+")
        redo = get_taxid(name_fixed[0] + " " + name_fixed[1])
        search2 = Entrez.esearch(term = name_fixed, db = "taxonomy", retmode = "xml")
    else:
        return record['IdList'][0]

#####GET DATA FROM TAXID THROUGH ENTREZ EFETCH####
def get_tax_data(taxid):
    """once we have the taxid, we can fetch the record"""
    search = Entrez.efetch(id = taxid, db = "taxonomy", retmode = "xml")
    return Entrez.read(search)

###GET
def get_tax_hierarchy(species):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in ['kingdom','phylum', 'class', 'order', 'family', 'genus']}
    return lineage

def get_tax_info(species, taxLevel):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [taxLevel]}
    return lineage




#####SEARCH BY GENE FUNCTION#####
def search_by_function(functionName, counts, levelOfInterest, userEmail):

    Entrez.email = userEmail
    Entrez.tool = "Vibhu-species2taxon"
    if not Entrez.email:
        print("you must add your email address")
        sys.exit(2)

    subset = counts['0'].map(lambda x: functionName in x)
    functionCountsTable = counts[subset] #creates the subset of the dataFrame
    taxid_list = [] # Initiate the lists to store the data to be parsed in
    data_list = []
    lineage_list = []
    
    print('parsing taxonomic data...') # message declaring the parser has begun

    for species in functionCountsTable['1']:
        print ('\t' + species) # progress messages

        #taxid = get_taxid(species) # Apply your functions
        #data = get_tax_data(taxid)
        #lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [levelOfInterest]}
        lineage = get_tax_hierarchy(species, levelOfInterest)
        #taxid_list.append(taxid) # Append the data to lists already initiated
        #data_list.append(data)
        lineage_list.append(str(lineage))
        time.sleep(5)

    #search = Entrez.efetch(id = taxid_list, db = "taxonomy", retmode = "xml")
    #record = Entrez.read(search)
    functionCountsTable['1'] = lineage_list
    groupedCounts = functionCountsTable.groupby('1').sum()
    print('complete!')
    return groupedCounts

###CONVERT COUNTS TO DISTRIBUTION###
def convert_to_distribution(lvlCounts):
        n = len(bin(len(lvlCounts['counts']))[2:])
        l = [bin(x)[2:].rjust(n, '0') for x in range(2**n)]
        counts_distribution = Distribution(l[:len(lvlCounts['counts'])], lvlCounts['counts'])
        return counts_distribution

###MAKE COUNTS PROPORTIONAL (FOR RENYI PROFILE ETC)###
def make_counts_proportional(counts):
        counts['2'] = counts['2']/counts['2'].sum()
        return counts

#####CALCULATE RENYI PROFILE#####
def calculate_renyi_profile(lvlCounts):
    counts_distribution = convert_to_distribution(lvlCounts)
    profile_values = []
    profile_values.append(renyi_entropy(counts_distribution, 0))
    profile_values.append(renyi_entropy(counts_distribution, .25))
    profile_values.append(renyi_entropy(counts_distribution, .5))
    profile_values.append(renyi_entropy(counts_distribution, 1))
    profile_values.append(renyi_entropy(counts_distribution, 2))
    profile_values.append(renyi_entropy(counts_distribution, 4))
    profile_values.append(renyi_entropy(counts_distribution, 8))
    profile_values.append(renyi_entropy(counts_distribution, 16))
    profile_values.append(renyi_entropy(counts_distribution, 32))
    profile_values.append(renyi_entropy(counts_distribution, 64))
    profile_values.append(renyi_entropy(counts_distribution, np.inf))
    return profile_values
