#!/usr/bin/python
import math
import numpy as np
import pandas as pd
import sys
import dit as dit
##import infotheo.py
from dit import Distribution
from Bio import Entrez
from dit.other import renyi_entropy
import matplotlib
import matplotlib.pyplot as plt


def get_counts_within_metagenome(fxnTable, orgTable):
    fxnData = pd.read_csv(fxnTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])
    orgData = pd.read_csv(orgTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])

    fxnDataMax = keep_max_pctID_only(fxnData)
    orgDataMax = keep_max_pctID_only(orgData)

    counts = counts_within_metagenome(fxnDataMax, orgDataMax)

    pCounts = make_counts_proportional(counts) #is this necessary at this step?

    return pCounts

'''

def read_annotation_table(tableFile):
    global table
    table = pd.read_csv(tableFile, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])
    return table

    #Read from organism and function tables output by MG-RAST
    fxnData = read_annotation_table(fxnTable)
    orgData = read_annotation_table(orgTable)

'''

def keep_max_pctID_only(table):
    mask = table.groupby('query sequence id').agg('idxmax')
    maxPctID = table.loc[mask['percentage identity']].reset_index()
    return maxPctID
'''
   #keep annotations with max percentage identity only
   fxnDataMax = keep_max_pctID_only(fxnData)
   orgDataMax = keep_max_pctID_only(orgData)
'''


def merge_annotation_tables(table1, table2):
    mergedTable = pd.merge(table1, table2, on = 'query sequence id').sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y'], ascending = [0,0])
    groupByQuery = mergedTable.groupby('query sequence id')
    return mergedTable
'''
    mergedTable = merge_annotation_tables(fxnDataMax, orgDataMax)
'''


def get_and_save_counts(mergedTable):
    counts1 = mergedTable.sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).groupby(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).size()
    counts1.to_csv('counts_of_metagenome.csv') #find how to customise string with either variable or
    counts = pd.read_csv('counts_of_metagenome.csv')
    counts.columns = ['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y', 'number of hits']
    return counts
'''
## get counts for the metagenome
    metagenomeCounts = get_and_save_counts(mergedTable)
'''


def read_and_filter_annotations(annotationTable):
    '''
    global tableData
    tableData = pd.read_csv(annotationTable)
    '''
    global tableDataMax
    tableDataMax = keep_max_pctID_only(annotationTable)
    return tableDataMax


def counts_within_metagenome(fxnTable, orgTable):
    fxnDataMax = read_and_filter_annotations(fxnTable) #treats the input file name like a string for some reason, but only within the function??
    orgDataMax = read_and_filter_annotations(orgTable) #treats the input file name like a string for some reason, but only within the function??
    mergedTable = merge_annotation_tables(fxnDataMax, orgDataMax)
    counts = get_and_save_counts(mergedTable)
    return counts


def make_counts_proportional(counts):
    counts['number of hits'] = counts['number of hits']/counts['number of hits'].sum()
    return counts



def search_by_function(functionName, counts, levelOfInterest, userEmail):

    Entrez.email = userEmail
    if not Entrez.email:
        print("you must add your email address")
        sys.exit(2)

    ''' 
    #using groups——requires exact name
    functionCountsTable = counts.groupby('semicolon separated list of annotations_x').get_group(functionName)
    '''
    
     

    functionCountsTable = counts[counts['semicolon separated list of annotations_x'].str.contains(functionName)]
     
    taxid_list = [] # Initiate the lists to store the data to be parsed in
    data_list = []
    lineage_list = []
    print('parsing taxonomic data...') # message declaring the parser has begun

    for species in functionCountsTable['semicolon separated list of annotations_y']:
        print(('\t' + species)) # progress messages

        taxid = get_taxid(species) # Apply your functions
        data = get_tax_data(taxid)
        lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [levelOfInterest]}
        taxid_list.append(taxid) # Append the data to lists already initiated
        data_list.append(data)
        lineage_list.append(str(lineage))

    print('complete!')
    functionCountsTable = functionCountsTable.assign(lineage_of_interest = lineage_list)
    speciesNames = functionCountsTable.pop('semicolon separated list of annotations_y')
    del functionCountsTable['semicolon separated list of annotations_x']
    lvlCounts = functionCountsTable.groupby('lineage_of_interest', as_index=False).sum()
    make_counts_proportional(lvlCounts)
    plot_counts(lvlCounts, levelOfInterest)
    return lvlCounts


def get_taxid(species):
    """to get data from ncbi taxomomy, we need to have the taxid.  we can get that by passing the species name to esearch, which will return the tax id"""
    species = species.split(';')[0].replace(" ", "+").strip()
    search = Entrez.esearch(term = species, db = "taxonomy", retmode = "xml")
    record = Entrez.read(search)
    return record['IdList'][0]

def get_tax_data(taxid):
    """once we have the taxid, we can fetch the record"""
    search = Entrez.efetch(id = taxid, db = "taxonomy", retmode = "xml")
    return Entrez.read(search)

def get_tax_hierarchy(species):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in ['kingdom','phylum', 'class', 'order', 'family', 'genus']}
    return lineage

def get_tax_info(species, taxLevel):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [taxLevel]}
    return lineage


def plot_counts(lvlCounts, lvlOfInterest):
    lineages = lvlCounts['lineage_of_interest']
    y_pos = np.arange(len(lineages))
    performance = lvlCounts['number of hits']

    plt.barh(y_pos, performance, align='center', alpha=0.4)
    plt.yticks(y_pos, lineages)
    plt.xlabel(lvlOfInterest)
    plt.title('Percentage by ' + lvlOfInterest)
    plt.show()
    return plt


def search_by_organism(organismName, normCountsTable):

    orgGroup = normCountsTable.groupby('semicolon separated list of annotations_y').get_group(organismName)

    return orgGroup




def convert_to_distribution(lvlCounts):
    n = len(bin(len(lvlCounts['number of hits']))[2:])
    l = [bin(x)[2:].rjust(n, '0') for x in range(2**n)]
    counts_distribution = Distribution(l[:len(lvlCounts['number of hits'])], lvlCounts['number of hits'])
    return counts_distribution



#This is used to calculate the Renyi entropy profile for a single metagenome after filtering by the gene/function of interest
def calculate_renyi_profile(lvlCounts):
    counts_distribution = convert_to_distribution(lvlCounts)
    profile_values = []
    profile_values.append(renyi_entropy(counts_distribution, 0))
    profile_values.append(renyi_entropy(counts_distribution, .25))
    profile_values.append(renyi_entropy(counts_distribution, .5))
    profile_values.append(renyi_entropy(counts_distribution, 1))
    profile_values.append(renyi_entropy(counts_distribution, 2))
    profile_values.append(renyi_entropy(counts_distribution, 4))
    profile_values.append(renyi_entropy(counts_distribution, 8))
    profile_values.append(renyi_entropy(counts_distribution, 16))
    profile_values.append(renyi_entropy(counts_distribution, 32))
    profile_values.append(renyi_entropy(counts_distribution, 64))
    profile_values.append(renyi_entropy(counts_distribution, np.inf))
    return profile_values

