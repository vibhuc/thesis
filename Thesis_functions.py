#!/usr/bin/python
import math
import numpy as np
import pandas as pd
import sys
##import infotheo.py
from dit import Distribution
from Bio import Entrez
from dit.other import renyi_entropy
import matplotlib
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier as RFC
from sklearn.ensemble import ExtraTreesClassifier as ETC
from sklearn.datasets import load_iris
from sklearn.cross_validation import cross_val_score
import glob
import os
import mygene
import time

def get_counts_within_metagenome(fxnTable, orgTable, groupName, sampleID):
    fxnData = pd.read_csv(fxnTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])
    orgData = pd.read_csv(orgTable, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])

    fxnDataMax = keep_max_pctID_only(fxnData)
    orgDataMax = keep_max_pctID_only(orgData)

    counts = counts_within_metagenome(fxnDataMax, orgDataMax, groupName, sampleID)

    #pCounts = make_counts_proportional(counts) #is this necessary at this step?

    return counts

'''
def read_annotation_table(tableFile):
    global table
    table = pd.read_csv(tableFile, delimiter = '\t', usecols=['query sequence id', 'percentage identity', 'bit score', 'semicolon separated list of annotations'])
    return table

    #Read from organism and function tables output by MG-RAST
    fxnData = read_annotation_table(fxnTable)
    orgData = read_annotation_table(orgTable)

'''

def keep_max_pctID_only(table):
    mask = table.groupby('query sequence id').agg('idxmax')
    maxPctID = table.loc[mask['percentage identity']].reset_index()
    return maxPctID
'''
   #keep annotations with max percentage identity only
   fxnDataMax = keep_max_pctID_only(fxnData)
   orgDataMax = keep_max_pctID_only(orgData)
'''


def merge_annotation_tables(table1, table2):
    mergedTable = pd.merge(table1, table2, on = 'query sequence id').sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y'], ascending = [0,0])
    groupByQuery = mergedTable.groupby('query sequence id')
    return mergedTable
'''
    mergedTable = merge_annotation_tables(fxnDataMax, orgDataMax)
'''


def get_and_save_counts(mergedTable, groupName, sampleID):
    counts1 = mergedTable.sort_values(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).groupby(['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y']).size()
    newFileName = '%(group)s_%(sample)s.csv' % \
        {"group": groupName, "sample": sampleID}  ####this is where I am 04:07AM  16 Aug
    counts1.to_csv(os.path.join('counts', newFileName)) #find how to customise string with either variable or
    counts = pd.read_csv(os.path.join('counts', newFileName)) ##set it so the file name is based on each dataframe's metagenome ID or some unique qualifier 
    counts.columns = ['semicolon separated list of annotations_x', 'semicolon separated list of annotations_y', 'number of hits']
    return counts
'''
## get counts for the metagenome
    metagenomeCounts = get_and_save_counts(mergedTable)
'''


def read_and_filter_annotations(annotationTable):
    '''
    global tableData
    tableData = pd.read_csv(annotationTable)
    '''
    global tableDataMax
    tableDataMax = keep_max_pctID_only(annotationTable)
    return tableDataMax


def counts_within_metagenome(fxnTable, orgTable, groupName, sampleID):
    fxnDataMax = read_and_filter_annotations(fxnTable) #treats the input file name like a string for some reason, but only within the function??
    orgDataMax = read_and_filter_annotations(orgTable) #treats the input file name like a string for some reason, but only within the function??
    mergedTable = merge_annotation_tables(fxnDataMax, orgDataMax)
    counts = get_and_save_counts(mergedTable, groupName, sampleID)
    return counts


def make_counts_proportional(counts):
    counts['counts'] = counts['counts']/counts['counts'].sum()
    return counts

def counts_by_gene_alone(counts, groupName, sampleID):
    #1. merge by total gene counts
    grouped = counts.groupby(['semicolon separated list of annotations_x'], as_index=False).aggregate(np.sum)
    #reset column names
    grouped.columns = range(grouped.shape[1])
    #2.add label row
    result = grouped #.append(pd.DataFrame(['Group', groupName]).T, ignore_index=True)
    newFileName = '%(group)s_%(sample)s.csv' % \
        {"group": groupName, "sample": sampleID}
    result.to_csv(os.path.join('gene-only_counts', newFileName)) #find how to customise string with either variable or
    return result


###next step: join the many dataframes at once (see: http://notconfusing.com/joining-many-dataframes-at-once-in-pandas-n-ary-join/)
def make_df(filename):
    df = pd.read_csv(filename, header=None, names=['gene name', 'organism name', 'counts'], dtype={'gene name': object, 'organism name': object, 'counts': np.float64})
    name = filename.split('.')[0]
    #df.columns = map(lambda col: '{}_{}'.format(str(col), name), df.columns)
    return df



def join_dfs(ldf, rdf):
    print( "merging " + rdf + "...")
    return ldf.merge(rdf, how = 'outer', on = ['0'])#is inner right for the type of join?

def join_dfs_on_2(ldf, rdf):
    print( "merging " + rdf + "...")
    return ldf.join(rdf, on=['gene name', 'organism name'], how='outer')#is inner right for the type of join?


def merge_all_gene_only_counts(pathToCountFiles):
    path = pathToCountFiles
    filenames = glob.glob(os.path.join(path, '*.csv'))
    dfs = [make_df(filename) for filename in filenames]
    final_df = reduce(join_dfs, dfs) #that's the magic
    final_gene_counts = final_df.T #transposed so it can go into the Random Forest
    final_gene_counts.columns = final_gene_counts.iloc[0]
    final_gene_counts = final_gene_counts[1:]
    return final_gene_counts


def merge_all_gene_and_org_counts(pathToCountFiles):
    path = pathToCountFiles
    filenames = glob.glob(os.path.join(path, '*.csv'))
    dfs = [make_df(filename) for filename in filenames]
    final_df = reduce(join_dfs_on_2, dfs) #that's the magic
    final_gene_counts = final_df.T #transposed so it can go into the Random Forest
    #final_gene_counts.columns = final_gene_counts.iloc[0]
    #final_gene_counts = final_gene_counts[1:]
    return final_gene_and_org_counts


    #merge_all_gene_and_org_counts(pathToCountFiles)

##### IMPLEMENTATION OF RANDOM FOREST #####
def predict_important_genes(final_gene_counts):

    #Creating training data column and add labels column
    final_gene_counts['is_train'] = np.random.uniform(0, 1, len(final_gene_counts)) <= .75
    
 
    #Separating into traing and testing sets with specific features
    train, test = final_gene_counts[final_gene_counts['is_train']==True], final_gene_counts[final_gene_counts['is_train']==False]
    features = final_gene_counts.columns[0:(len(list(final_gene_counts.columns.values))-2)]

    #Random Forest Classifier with n_jobs set to 2 and 5000 trees max
    forest = RFC(n_jobs=4,n_estimators=50)
    y, _ = pd.factorize(train['Group'])
    forest.fit(train[features], y)


    #Print prediction results 
    preds = final_gene_counts['Group'].unique().target_names[forest.predict(test[features])] ##need to fix
    print pd.crosstab(index=test['Group'], columns=preds, rownames=['actual'], colnames=['preds'])


    #Display feature importances 
    importances = forest.feature_importances_
    #indices = np.argsort(importances)
    indices = np.argsort(importances)[::-1]

    # Print the feature ranking
    print("Feature ranking:")

    for f in range(train[features].shape[1]):
        print("%d. feature %d (%f)" % (f + 1, indices[f], importances[indices[f]]))


    #plt.figure(1)
    #plt.title('Likely Important Genes')
    #plt.barh(range(len(indices)), importances[indices], color='b', align='center')
    #plt.yticks(range(len(indices)), features[indices])
    #plt.xlabel('Relative Importance')
    #plt.show()
    return indices



def search_by_function(functionName, counts, levelOfInterest, userEmail):

    Entrez.email = userEmail
    Entrez.tool = "Vibhu-species2taxon"
    if not Entrez.email:
        print "you must add your email address"
        sys.exit(2)

    ''' 
    #using groups requires exact name
    functionCountsTable = counts.groupby('semicolon separated list of annotations_x').get_group(functionName)
    '''
    
     

    functionCountsTable = counts[counts['gene name'].str.contains(functionName)] #creates the subset of the dataFrame
    taxid_list = [] # Initiate the lists to store the data to be parsed in
    data_list = []
    lineage_list = []
    print('parsing taxonomic data...') # message declaring the parser has begun

    for species in functionCountsTable['organism name']:
        print ('\t' + species) # progress messages

        taxid = get_taxid(species) # Apply your functions
        #data = get_tax_data(taxid)
        #lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [levelOfInterest]}
        taxid_list.append(taxid) # Append the data to lists already initiated
        #data_list.append(data)
        #lineage_list.append(str(lineage))
        time.sleep(1)

    search = Entrez.efetch(id = taxid_list, db = "taxonomy", retmode = "xml")
    record = Entrez.read(search)
    print('complete!')
    return taxid_list

#TEMPORARY UNTIL I FIX THE EFETCH
    #functionCountsTable = functionCountsTable.assign(lineage_of_interest = lineage_list)
    #speciesNames = functionCountsTable.pop('organism name')
    #del functionCountsTable['gene name']
    #lvlCounts = functionCountsTable.groupby('lineage_of_interest', as_index=False).sum()
    #make_counts_proportional(lvlCounts)
    #plot_counts(lvlCounts, levelOfInterest)
    #return lvlCounts

def search_by_function_m(functionName, counts, levelOfInterest, userEmail):

    Entrez.email = userEmail
    if not Entrez.email:
        print "you must add your email address"
        sys.exit(2)

    ''' 
    #using groups requires exact name
    functionCountsTable = counts.groupby('semicolon separated list of annotations_x').get_group(functionName)
    '''
    
     

    functionCountsTable = counts[counts['0'].str.contains(functionName)]
     
    taxid_list = [] # Initiate the lists to store the data to be parsed in
    data_list = []
    lineage_list = []
    print('parsing taxonomic data...') # message declaring the parser has begun

    for species in list(functionCountsTable['1']):
        print ('\t' + species) # progress messages

        taxid = get_taxid(species) # Apply your functions
        data = get_tax_data(taxid)
        lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [levelOfInterest]}
        taxid_list.append(taxid) # Append the data to lists already initiated
        data_list.append(data)
        lineage_list.append(str(lineage))

    print('complete!')
    functionCountsTable = functionCountsTable.assign(lineage_of_interest = lineage_list)
    speciesNames = functionCountsTable.pop('1')
    del functionCountsTable['0']
    lvlCounts = functionCountsTable.groupby('lineage_of_interest', as_index=False).sum()
    make_counts_proportional(lvlCounts)
    plot_counts(lvlCounts, levelOfInterest)
    return lvlCounts





def get_taxid(species):
    """to get data from ncbi taxomomy, we need to have the taxid.  we can get that by passing the species name to esearch, which will return the tax id"""
    species = species.split(';')[0].replace(" ", "+").split('+')
    if species[0] == 'Candidatus':
        species = species[0] + ' ' + species[1]
    elif species[1] == 'proteobacterium' or species[1] == 'actinobacterium':
        species = species[0] + ' ' + species[1] + ' ' + species[2]
    elif species == ['uncultured', 'bacterium']:
        species = species[0] + ' ' + species[1]
    elif ('marine' in species) or ('uncultured' in species):
        species = ' '.join(species)
    else:
        species = species[0]
    search = Entrez.esearch(term = species, db = "taxonomy", retmode = "xml", usehistory = "y")
    record = Entrez.read(search)
    if (record['IdList'] == []):
        name_fixed = record['ErrorList']['PhraseNotFound'][0].split("+")
        redo = get_taxid(name_fixed[0] + " " + name_fixed[1])
        search2 = Entrez.esearch(term = name_fixed, db = "taxonomy", retmode = "xml")
    else:
        return record['IdList'][0]


def get_tax_data(taxid):
    """once we have the taxid, we can fetch the record"""
    search = Entrez.efetch(id = taxid, db = "taxonomy", retmode = "xml")
    return Entrez.read(search)

def get_tax_hierarchy(species):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in ['kingdom','phylum', 'class', 'order', 'family', 'genus']}
    return lineage

def get_tax_info(species, taxLevel):
    taxid = get_taxid(species) #get taxonomic data
    data = get_tax_data(taxid)
    lineage = {d['Rank']:d['ScientificName'] for d in data[0]['LineageEx'] if d['Rank'] in [taxLevel]}
    return lineage


def plot_counts(lvlCounts, lvlOfInterest):
    lineages = lvlCounts['lineage_of_interest']
    y_pos = np.arange(len(lineages))
    performance = lvlCounts['counts']

    plt.barh(y_pos, performance, align='center', alpha=0.4)
    plt.yticks(y_pos, lineages)
    plt.xlabel(lvlOfInterest)
    plt.title('Percentage by ' + lvlOfInterest)
    plt.show()
    return plt

'''
def plot_histogram_of_counts(lvlCounts, lvlOfInterest):
    plt.hist(beta(a, b, size=10000), histtype="stepfilled",
             bins=25, alpha=0.8, normed=True)
'''



def search_by_organism(organismName, normCountsTable):

    orgGroup = normCountsTable.groupby('semicolon separated list of annotations_y').get_group(organismName)

    return orgGroup




def convert_to_distribution(lvlCounts):
    n = len(bin(len(lvlCounts['counts']))[2:])
    l = [bin(x)[2:].rjust(n, '0') for x in range(2**n)]
    counts_distribution = Distribution(l[:len(lvlCounts['counts'])], lvlCounts['counts'])
    return counts_distribution



#This is used to calculate the Renyi entropy profile for a single metagenome after filtering by the gene/function of interest
def calculate_renyi_profile(lvlCounts):
    counts_distribution = convert_to_distribution(lvlCounts)
    profile_values = []
    profile_values.append(renyi_entropy(counts_distribution, 0))
    profile_values.append(renyi_entropy(counts_distribution, .25))
    profile_values.append(renyi_entropy(counts_distribution, .5))
    profile_values.append(renyi_entropy(counts_distribution, 1))
    profile_values.append(renyi_entropy(counts_distribution, 2))
    profile_values.append(renyi_entropy(counts_distribution, 4))
    profile_values.append(renyi_entropy(counts_distribution, 8))
    profile_values.append(renyi_entropy(counts_distribution, 16))
    profile_values.append(renyi_entropy(counts_distribution, 32))
    profile_values.append(renyi_entropy(counts_distribution, 64))
    profile_values.append(renyi_entropy(counts_distribution, np.inf))
    return profile_values







