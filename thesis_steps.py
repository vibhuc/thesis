#!/usr/bin/python

from Thesis_functions import *



#For LT0 metagenomes
counts1LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664461.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664461.3_organism_GenBank.tab", 'LT0', '1')
geneCounts1LT0 = counts_by_gene_alone(counts1LT0, 'LT0', '1')

counts2LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664466.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664466.3_organism_GenBank.tab", 'LT0', '2')
geneCounts2LT0 = counts_by_gene_alone(counts2LT0, 'LT0', '2')

counts3LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664471.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664471.3_organism_GenBank.tab", 'LT0', '3')
geneCounts3LT0 = counts_by_gene_alone(counts3LT0, 'LT0', '3')

#wait on this one
counts4LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664474.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664474.3_organism_GenBank.tab", 'LT0', '4')
geneCounts4LT0 = counts_by_gene_alone(counts4LT0, 'LT0', '4')

counts5LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664475.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664475.3_organism_GenBank.tab", 'LT0', '5')
geneCounts5LT0 = counts_by_gene_alone(counts5LT0, 'LT0', '5')

counts6LT0 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT0/mgm4664478.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT0/mgm4664478.3_organism_GenBank.tab", 'LT0', '6')
geneCounts6LT0 = counts_by_gene_alone(counts6LT0, 'LT0', '6')



#For LT5 Metagenomes
counts1LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664455.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664455.3_organism_GenBank.tab", 'LT5', '1')
geneCounts1LT5 = counts_by_gene_alone(counts1LT5, 'LT5', '1')

counts2LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664456.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664456.3_organism_GenBank.tab", 'LT5', '2')
geneCounts2LT5 = counts_by_gene_alone(counts2LT5, 'LT5', '2')

counts3LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664464.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664464.3_organism_GenBank.tab", 'LT5', '3')
geneCounts3LT5 = counts_by_gene_alone(counts3LT5, 'LT5', '3')

counts4LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664468.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664468.3_organism_GenBank.tab", 'LT5', '4')
geneCounts4LT5 = counts_by_gene_alone(counts4LT5, 'LT5', '4')

counts5LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664470.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664470.3_organism_GenBank.tab", 'LT5', '5')
geneCounts5LT5 = counts_by_gene_alone(counts5LT5, 'LT5', '5')

counts6LT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/LT5/mgm4664472.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/LT5/mgm4664472.3_organism_GenBank.tab", 'LT5', '6')
geneCounts6LT5 = counts_by_gene_alone(counts6LT5, 'LT5', '6')





#For PT5 Metagenomes
counts1PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664458.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664458.3_organism_GenBank.tab", 'PT5', '1')
geneCounts1PT5 = counts_by_gene_alone(counts1PT5, 'PT5', '1')

counts2PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664462.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664462.3_organism_GenBank.tab", 'PT5', '2')
geneCounts2PT5 = counts_by_gene_alone(counts2PT5, 'PT5', '2')

counts3PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664463.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664463.3_organism_GenBank.tab", 'PT5', '3')
geneCounts3PT5 = counts_by_gene_alone(counts3PT5, 'PT5', '3')

counts4PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664469.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664469.3_organism_GenBank.tab", 'PT5', '4')
geneCounts4PT5 = counts_by_gene_alone(counts4PT5, 'PT5', '4')

counts5PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664473.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664473.3_organism_GenBank.tab", 'PT5', '5')
geneCounts5PT5 = counts_by_gene_alone(counts5PT5, 'PT5', '5')

counts6PT5 = get_counts_within_metagenome("~/thesis/Thesis Metagenomes/PT5/mgm4664477.3_function_GenBank.tab", "~/thesis/Thesis Metagenomes/PT5/mgm4664477.3_organism_GenBank.tab", 'PT5', '6')
geneCounts6PT5 = counts_by_gene_alone(counts6PT5, 'PT5', '6')







'''
#Merging All Gene Counts
final_gene_counts = merge_all_dfs("gene-only_counts/")


final_gene_counts['is_train'] = np.random.uniform(0, 1, len(final_gene_counts)) <= .85
    
 
    #Separating into traing and testing sets with specific features
train, test = final_gene_counts[final_gene_counts['is_train']==True], final_gene_counts[final_gene_counts['is_train']==False]
features = final_gene_counts.columns[0:(len(list(final_gene_counts.columns.values))-2)]

    #Random Forest Classifier with n_jobs set to 2 and 5000 trees max
#forest = RFC(n_jobs=8,n_estimators=10000, min_samples_split=3, max_features=None)
forest = ETC(n_estimators=5000, min_samples_split=3, max_features=None)
y, _ = pd.factorize(train['Group'])
forest.fit(train[features], y)


groupNames = np.array(final_gene_counts['Group'].unique())
#Print prediction results
print "Testing Set" 
preds = groupNames[forest.predict(test[features])] ##need to fix
print pd.crosstab(index=test['Group'], columns=preds, rownames=['actual'], colnames=['preds'])

print "Training Set" 
preds2 = groupNames[forest.predict(train[features])] ##need to fix
print pd.crosstab(index=train['Group'], columns=preds2, rownames=['actual'], colnames=['preds'])

scores = cross_val_score(forest, train[features], y)
scores.mean()

#Display feature importances 
importances = forest.feature_importances_
#indices = np.argsort(importances)
indices = np.argsort(importances)[::-1]


plt.figure(1)
plt.title('Feature Importances')
plt.barh(range(50), importances[indices][0:50], color='b', align='center')
plt.yticks(range(50), features[indices][0:50])
plt.xlabel('Relative Importance')
'''




'''
counts_within_class = search_by_function("2-amino-3-ketobutyrate", counts, 'class', 'vibhu@outlook.com')

renyi_by_class = calculate_renyi_profile(counts_within_class)
'''